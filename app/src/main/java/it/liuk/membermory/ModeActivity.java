package it.liuk.membermory;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.bumptech.glide.Glide;
import com.github.jinatonic.confetti.CommonConfetti;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class ModeActivity extends AppCompatActivity {
    Intent intent;
    ConstraintLayout container;
    Button firstButton, secondButton;
    TextView resultText;
    ImageView gameImage;
    int value;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mode);
        container = findViewById(R.id.container_mode);
        resultText = findViewById(R.id.resultText);
        gameImage = findViewById(R.id.gameImage);
        firstButton = findViewById(R.id.firstButton);
        secondButton = findViewById(R.id.secondButton);

        intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle == null) {
            finish();
        }
        value = bundle.getInt("MODE");
        Utilities.instance(this);

        if (value == 1 || value == 5) {
            //Risposta Esatta
            resultText.setText(getApplicationContext().getResources().getText(R.string.correct));

            Glide.with(getBaseContext())
                    .load(R.drawable.next)
                    .fitCenter()
                    .into(gameImage);

            firstButton.setText(getApplicationContext().getResources().getText(R.string.next));
            gameImage.setOnClickListener(view -> setFirstButton());
        } else if (value == 3) {
            resultText.setText(getApplicationContext().getResources().getText(R.string.win));
            Glide.with(getBaseContext())
                    .load(R.drawable.win)
                    .fitCenter()
                    .into(gameImage);
            firstButton.setText("Classifica");
            new Handler().postDelayed(() -> runOnUiThread(() ->
                    CommonConfetti.rainingConfetti(container, new int[]{Color.YELLOW, Color.RED, Color.BLUE, Color.GREEN}).stream(7000)), 100);
        } else {
            if (value == 2) {
                resultText.setText(getApplicationContext().getResources().getText(R.string.wrongans));
                Glide.with(getBaseContext())
                        .load(R.drawable.wrong)
                        .fitCenter()
                        .into(gameImage);
            } else if (value == 4) {
                resultText.setText(getApplicationContext().getResources().getText(R.string.timeup));
                Glide.with(getBaseContext())
                        .load(R.drawable.wrong)
                        .fitCenter()
                        .into(gameImage);
            }
            firstButton.setText(getApplicationContext().getResources().getText(R.string.play_again));
        }


        firstButton.setOnClickListener(view -> setFirstButton());
        secondButton.setText(getApplicationContext().getResources().getText(R.string.menu));
        secondButton.setVisibility(View.VISIBLE);
        secondButton.setOnClickListener(view -> {
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
            finish();
        });
    }

    public void setFirstButton(){
        if (value == 1 || value == 5){
            finishActivity(1);
            finish();
        } else if(value == 3) {
            startActivity(new Intent(getApplicationContext(), RankingActivity.class));
            finish();
        } else if (value == 2 || value == 4){
            startActivity(new Intent(getApplicationContext(), GameActivity.class));
            finish();
        }
    }


    @Override
    public void onBackPressed() {
        SweetAlertDialog dialog = new SweetAlertDialog(this);
        dialog.setTitleText("Tornare al Menù?");
        dialog.setContentText("Se non hai raggiunto un livello perderai i tuoi ultimi progressi.");
        dialog.setConfirmButton("Si", sweetAlertDialog -> {
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
            finish();
        });
        dialog.setCancelable(true);
        dialog.setCancelText("Annulla");
        dialog.show();
    }
}
