package it.liuk.membermory;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class GameActivity extends AppCompatActivity {

    static final int CORRECT_ANSWER = 1;
    static final int WRONG_ANSWER = 2;
    static final int WIN_GAME = 3;
    static final int TIME_UP = 4;
    static final int NEXT_LEVEL = 5;

    private FirebaseDatabase fd = FirebaseDatabase.getInstance();
    private DatabaseReference animals = fd.getReference().child("animals");
    int idx, timeValue, size;
    String Answer;

    ImageView imageView, scoreView;
    Button a, b, c, d;
    ProgressBar timeBar, loadingBar;
    TextView timeText, scoreText;
    CountDownTimer timer;
    List<View> buttonView;
    int score;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        imageView = findViewById(R.id.gameImage);
        scoreView = findViewById(R.id.coinImage);
        a = findViewById(R.id.aBtn);
        b = findViewById(R.id.bBtn);
        c = findViewById(R.id.cBtn);
        d = findViewById(R.id.dBtn);
        timeText = findViewById(R.id.timeText);
        scoreText = findViewById(R.id.lvText);
        timeBar = (ProgressBar)findViewById(R.id.timeBar);
        loadingBar = findViewById(R.id.loadingBar);
        loadingBar.setVisibility(View.VISIBLE);
        buttonView = Arrays.asList(a,b,c,d);


        resetColor();
        Utilities.instance(this.getApplicationContext());
        Utilities.instance().checkInternet(this);
        int temp = Utilities.instance().fetchValueInt("level");
        idx = (temp > 0) ? (temp*10)-1 : 0;
        score = Utilities.instance().fetchValueInt("score");
        scoreText.setText(String.valueOf(score));

        a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                answer(v, a.getText().toString());
            }
        });

        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                answer(v, b.getText().toString());
            }
        });

        c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                answer(v, c.getText().toString());
            }
        });

        d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                answer(v, d.getText().toString());
            }
        });

        size = 0;
        animals.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                dataSnapshot.getChildren().forEach(ds -> {
                    size++;
                });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        updateQuestion(String.valueOf(idx));

        timeValue = 10;
        timeText.setText("10\"");
        timer = new CountDownTimer(10000, 1000) {
            public void onTick(long millisUntilFinished) {

                timeText.setText(String.valueOf(timeValue) + "\"");

                if (timeValue < 0) {
                    timeText.setText("");
                }else{
                    timeValue--;
                }
            }
            public void onFinish() {
                timeUp();
            }
        };
        timer.cancel();
    }

    @Override
    protected void onStart() {
        super.onStart();
        restartTimer();
    }

    public void restartTimer(){
        timeValue = 10;
        timeText.setText("10\"");
        timer.cancel();
        timer.start();
    }

    public void timeUp() {
        timer.cancel();
        startActivity(new Intent(getApplicationContext(), ModeActivity.class)
                .putExtra("MODE", TIME_UP));
        finish();
    }
    @Override
    public void onPause() {
        super.onPause();
        timer.cancel();
    }

    @Override
    public void onResume() {
        super.onResume();
        timer.start();
    }

    @Override
    public void onBackPressed() {
        SweetAlertDialog dialog = new SweetAlertDialog(this);
        dialog.setTitleText("Tornare al Menù?");
        dialog.setContentText("Se non hai raggiunto un livello perderai i tuoi ultimi progressi.");
        dialog.setConfirmButton("Si", sweetAlertDialog -> {
            timer.cancel();
            finish();
        });
        dialog.setCancelable(true);
        dialog.setCancelText("Annulla");
        dialog.show();
    }

    public void updateQuestion(String child){
        scoreText.setText(String.valueOf(score));
        visibleUI(false);
        //aggiorna immagine
        animals.child(child).child("Url").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String url = dataSnapshot.getValue().toString();
                url = "https://firebasestorage.googleapis.com/v0/b/membermory.appspot.com/o/"+ url+ "?alt=media";

                Glide.with(getBaseContext())
                        .load(url)
                        .fitCenter()
                        .listener(new RequestListener<Drawable>() {

                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                restartTimer();
                                visibleUI(true);
                                return false;
                            }
                        })
                        .into(imageView);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            };
        });

        //risposta esatta
        Answer = "";
        animals.child(child).child("Corretta").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Answer = dataSnapshot.getValue().toString();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        //aggiorna le risposte
        animals.child(child).child("Risposte").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                List<String> answers = Arrays.asList(dataSnapshot.getValue().toString().split(","));
                Collections.shuffle(answers);
                a.setText(answers.get(0));
                b.setText(answers.get(1));
                c.setText(answers.get(2));
                d.setText(answers.get(3));
                if ((idx + 1) % 10 == 0) { //Ogni 10 salva il livello
                    saveLevel();
                }
                restartTimer();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void saveLevel(){
        String lv = String.valueOf((idx + 1)/10);
        Utilities.instance().storeValueInt("level", (idx + 1)/10);
        Utilities.instance().storeValueInt("score", score);
        Utilities.instance().SaveLevel(lv, String.valueOf(score));
        Toast toast = Toast.makeText(getApplicationContext(), "Livello " + lv, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.TOP, 0, 0);
        toast.show();
    }

    public void visibleUI(boolean cond){
        if (cond){
            scoreView.setVisibility(View.VISIBLE);
            scoreText.setVisibility(View.VISIBLE);
            loadingBar.setVisibility(View.GONE);
            timeBar.setVisibility(View.VISIBLE);
            timeText.setVisibility(View.VISIBLE);
            for (View v : buttonView) {
                v.setVisibility(View.VISIBLE);
            }
            timeBar.setVisibility(View.VISIBLE);
        }else{
            scoreView.setVisibility(View.INVISIBLE);
            scoreText.setVisibility(View.INVISIBLE);
            loadingBar.setVisibility(View.VISIBLE);
            timeBar.setVisibility(View.INVISIBLE);
            timeText.setVisibility(View.INVISIBLE);
            for (View v : buttonView) {
                v.setVisibility(View.INVISIBLE);
            }
            timeBar.setVisibility(View.INVISIBLE);
        }
    }

    public void answer(View v, String ans){
        timer.cancel();
        if (ans.equals(Answer)){
            Toast.makeText(getApplicationContext(), "Esatto!", Toast.LENGTH_SHORT).show();
            v.getBackground().setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.lightGreen), PorterDuff.Mode.SRC_ATOP);

            int time = timeValue;
            score += (time > 2) ? (int) (time / 2) : 1;
            if (idx >= size - 1){
                gameWon();
            } else {
                if ((idx + 1) % 10 == 0) { //Ogni 10 salva il livello
                    startActivityForResult(
                            new Intent(getApplicationContext(), ModeActivity.class)
                                    .putExtra("MODE", NEXT_LEVEL), CORRECT_ANSWER);
                }else{
                    startActivityForResult(
                            new Intent(getApplicationContext(), ModeActivity.class)
                                    .putExtra("MODE", CORRECT_ANSWER), CORRECT_ANSWER);
                }
            }
        } else{
            timer.cancel();
            Toast.makeText(getApplicationContext(), "Sbagliato!", Toast.LENGTH_SHORT).show();
            v.getBackground().setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.backGround), PorterDuff.Mode.SRC_ATOP);

            startActivity(new Intent(getApplicationContext(), ModeActivity.class)
                            .putExtra("MODE", WRONG_ANSWER));
            finish();
        }
    }


    public void gameWon() {
        timer.cancel();
        startActivity(new Intent(getApplicationContext(), ModeActivity.class)
                .putExtra("MODE", WIN_GAME));
        finish();
    }


    public void resetColor() {
        for (View v : buttonView) {
            v.setBackgroundResource(R.drawable.radius);
            v.getBackground().setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.foreGround), PorterDuff.Mode.SRC_ATOP);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CORRECT_ANSWER && resultCode == RESULT_CANCELED){
            updateQuestion(String.valueOf(++idx));
            resetColor();
        }
    }
}
