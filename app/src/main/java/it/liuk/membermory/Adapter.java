package it.liuk.membermory;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder> {

    private List<User> userList;

    public Adapter(List<User> list) {
        this.userList = list;
    }

    public List<Integer> toList(Map<String,Integer> map){
        return map.entrySet().stream().map(Map.Entry::getValue).collect(Collectors.toList());
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.entry_layout, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        viewHolder.user_value.setText(userList.get(position).getName());
        viewHolder.level_value.setText(String.valueOf(userList.get(position).getLevel() + ""));
        viewHolder.score_value.setText(String.valueOf(userList.get(position).getScore() + ""));
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        private TextView user_value;
        private TextView level_value;
        private TextView score_value;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            user_value = itemView.findViewById(R.id.user_value);
            level_value = itemView.findViewById(R.id.level_value);
            score_value = itemView.findViewById(R.id.score_value);
        }
    }
}