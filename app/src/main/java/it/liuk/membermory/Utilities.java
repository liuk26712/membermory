package it.liuk.membermory;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class Utilities extends AppCompatActivity {

    static Utilities _instance;
    Context context;
    SharedPreferences sharedPref;
    SharedPreferences.Editor sharedPrefEditor;

    public static Utilities instance(Context context) {
        if (_instance == null) {
            _instance = new Utilities();
            _instance.configSessionUtils(context);
        }
        return _instance;
    }

    public static Utilities instance() {
        return _instance;
    }

  public void checkInternet(Context context){
      ConnectivityManager cm =
              (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

      NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

      if (activeNetwork == null || !activeNetwork.isConnectedOrConnecting()) {
          SweetAlertDialog sad = new SweetAlertDialog(context, SweetAlertDialog.NORMAL_TYPE);
                  sad.setTitleText("Connessione Assente");
                  sad.setContentText("Connettersi alla rete internet per continuare");
                  sad.setConfirmButton(android.R.string.ok, new SweetAlertDialog.OnSweetClickListener() {
                      @Override
                      public void onClick(SweetAlertDialog sweetAlertDialog) {
                          checkInternet(context);
                          sweetAlertDialog.dismissWithAnimation();
                      }
                  }).show();
      }
  }

    public void configSessionUtils(Context context) {
        this.context = context;
        sharedPref = context.getSharedPreferences("AppPreferences", MODE_PRIVATE);
        sharedPrefEditor = sharedPref.edit();
    }

    public void storeValueString(String key, String value) {
        sharedPrefEditor.putString(key, value);
        sharedPrefEditor.commit();
    }

    public void storeValueInt(String key, int value) {
        sharedPrefEditor.putInt(key, value);
        sharedPrefEditor.commit();
    }

    public void SaveLevel(String level, String score){
        String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        DocumentReference documentReference = FirebaseFirestore.getInstance().collection("users")
                .document(userId);
        Map<String, Object> user = new HashMap<>();
        user.put("fName", fetchValueString("fName"));
        user.put("email", fetchValueString("email"));
        user.put("level", level);
        user.put("score", score);
        documentReference.update(user).addOnSuccessListener(aVoid -> {
            Log.d("TAG", "onSuccess: utente aggiornato per " + userId);
        }).addOnFailureListener(e -> {
            Log.d("TAG", "onFailure: " + e.toString());
        });
    }

    public int getLevel() {
        int level;
        final CharSequence[] lv = new CharSequence[1];

        final FirebaseFirestore[] db = {FirebaseFirestore.getInstance()};
        FirebaseUser userCurr = FirebaseAuth.getInstance().getCurrentUser();
        String userId = userCurr.getUid();

        DocumentReference docRef = db[0].collection("users").document(userId);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document != null) {
                        lv[0] = ((CharSequence) document.getData().get("fName"));
                    } else {
                        Log.d("LOGGER", "No such document");
                    }
                } else {
                    Log.d("LOGGER", "get failed with ", task.getException());
                }
            }
        });

        return Integer.parseInt(lv[0].toString());
    }



    public String fetchValueString(String key) {
        return sharedPref.getString(key, "");
    }

    public int fetchValueInt(String key) {
        return sharedPref.getInt(key, 0);
    }
}
