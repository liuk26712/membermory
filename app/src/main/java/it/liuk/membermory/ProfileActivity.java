package it.liuk.membermory;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends AppCompatActivity {
    private static final int PICK_IMAGE = 1;
    TextView fullName,email, verifyMsg, reset;
    CircleImageView profileImage;
    FirebaseAuth fAuth;
    FirebaseFirestore fStore;
    String userId;
    Button resendCode, backMenu, changeProfile;
    FirebaseUser user;
    StorageReference storageReference;
    ProgressBar loadingBar;
    Uri imageUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        fullName = findViewById(R.id.profileName);
        email = findViewById(R.id.profileEmail);
        profileImage = findViewById(R.id.profileImage);
        backMenu = findViewById(R.id.backMenu);
        changeProfile = findViewById(R.id.profileChange);
        resendCode = findViewById(R.id.resendCode);
        verifyMsg = findViewById(R.id.verifyMsg);
        loadingBar = findViewById(R.id.loadingBar);
        reset = findViewById(R.id.resetView);

        Utilities.instance(this.getApplicationContext());
        Utilities.instance().checkInternet(this);

        fAuth = FirebaseAuth.getInstance();
        if (fAuth.getCurrentUser() == null) {
            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            finish();
        }

        SpannableString content = new SpannableString(getString(R.string.reset));
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        reset.setText(content);

        fStore = FirebaseFirestore.getInstance();
        storageReference = FirebaseStorage.getInstance().getReference();

        StorageReference profileRef = storageReference.child("users/" + fAuth.getCurrentUser().getUid() + "/profile.jpg");
        profileRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Glide.with(getBaseContext())
                        .load(uri)
                        .fitCenter()
                        .into(profileImage);
            }
        });

        userId = fAuth.getCurrentUser().getUid();
        user = fAuth.getCurrentUser();

        if (!user.isEmailVerified()) {

            verifyMsg.setVisibility(View.VISIBLE);
            resendCode.setVisibility(View.VISIBLE);

            resendCode.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {

                    user.sendEmailVerification().addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Toast toast = Toast.makeText(getApplicationContext(), "Email di verifica inviata.  Riesegui l'accesso", Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.TOP, 0, 0);
                            toast.show();
                            FirebaseAuth.getInstance().signOut();//logout
                            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                            finish();
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.d("tag", "onFailure: Email non inviata." + e.getMessage());
                        }
                    });
                }
            });
        } else {
            profileImage.setOnClickListener(v -> changeImageProfile());
            changeProfile.setOnClickListener(v -> changeImageProfile());
        }

        fullName.setText(Utilities.instance().fetchValueString("fName"));
        email.setText(Utilities.instance().fetchValueString("email"));

        backMenu.setOnClickListener(v -> finish());
    }

    public void resetScore(View view){
        SweetAlertDialog dialog = new SweetAlertDialog(this);
        dialog.setTitleText("Resettare il porio punteggio?");
        dialog.setContentText("Perderai tutti i tuoi progressi di gioco.");
        dialog.setConfirmButton("Si", sweetAlertDialog -> {
            Utilities.instance().SaveLevel("0", "0");
            Toast.makeText(this, "Punteggio azzerato", Toast.LENGTH_SHORT).show();
            sweetAlertDialog.dismissWithAnimation();
        });
        dialog.setCancelable(true);
        dialog.setCancelText("Annulla");
        dialog.show();
    }

    public void changeImageProfile(){
        Intent gallery = new Intent();
        gallery.setType("image/*");
        gallery.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(gallery, "Seleziona Immagine"), PICK_IMAGE);
    }

    @Override
    public void onStart(){
        super.onStart();
    }

    public void uploadImage(Bitmap image){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        StorageReference profileRef = storageReference.child("users/"+fAuth.getCurrentUser().getUid()+"/profile.jpg");

        image.compress(Bitmap.CompressFormat.JPEG, 100, baos);

        byte[] img = baos.toByteArray();
        UploadTask upload = profileRef.putBytes(img);

        loadingBar.setVisibility(View.VISIBLE);

        upload.addOnCompleteListener(u -> {
            loadingBar.setVisibility(View.GONE);
            if (u.isSuccessful()){
                profileRef.getDownloadUrl().addOnCompleteListener(urlTask -> {
                    imageUri = urlTask.getResult();
                    profileImage.setImageBitmap(image);
                });
            }else{
                Toast.makeText(getApplicationContext(), "Errore ! " + u.getException().getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE && resultCode == RESULT_OK && data != null) {
            imageUri = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
                uploadImage(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
