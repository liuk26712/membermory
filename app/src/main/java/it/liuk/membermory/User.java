package it.liuk.membermory;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class User {
    private String name;
    private int level;
    private int score;

    public User(){}

    public User(String fName, int level, int score)
    {
        this.name = fName;
        this.level = level;
        this.score = score;
    }

    public String getName() {
        return name;
    }

    public int getLevel() {
        return level;
    }

    public int getScore(){
       return score;
    }

    public void setScore(int score){
        this.score = score;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("fName", name);
        result.put("level", level);
        result.put("score", score);
        return result;
    }

}
