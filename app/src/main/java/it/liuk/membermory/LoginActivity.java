package it.liuk.membermory;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.BounceInterpolator;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class LoginActivity extends AppCompatActivity {
    EditText mEmail,mPassword;
    Button mLoginBtn;
    TextView mRegisterBtn, forgotTextLink;
    ImageView logoImage;
    ProgressBar progressBar;
    FirebaseAuth fAuth;
    CheckBox saveCheck;
    CountDownTimer timer;
    int time = 5;
    int dir = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mEmail = findViewById(R.id.Email);
        mPassword = findViewById(R.id.password);
        progressBar = findViewById(R.id.progressBar);
        fAuth = FirebaseAuth.getInstance();
        mLoginBtn = findViewById(R.id.loginBtn);
        mRegisterBtn = findViewById(R.id.registerBtn);
        forgotTextLink = findViewById(R.id.forgotPassword);
        saveCheck = findViewById(R.id.saveCheck);
        logoImage = findViewById(R.id.logoImage);

        animation();
        timer = new CountDownTimer(5000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                if (time > 0){
                    time--;
                }
            }

            @Override
            public void onFinish() {
                restartTimer();
            }
        };
        timer.start();

        Utilities.instance(this.getApplicationContext());
        Utilities.instance().checkInternet(this);

        if(fAuth.getCurrentUser() != null){
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
            finish();
        }

        progressBar.setVisibility(View.INVISIBLE);
        mLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String email = mEmail.getText().toString().trim();
                String password = mPassword.getText().toString().trim();

                if(TextUtils.isEmpty(email)){
                    mEmail.setError("Inserire una Email valida.");
                    return;
                }

                if(TextUtils.isEmpty(password)){
                    mPassword.setError("Inserire una Password valida.");
                    return;
                }

                if(password.length() < 6){
                    mPassword.setError("La Password deve essere maggiore di 6 caratteri.");
                    return;
                }

                progressBar.setVisibility(View.VISIBLE);

                fAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            Toast.makeText(LoginActivity.this, "Autenticato con successo!", Toast.LENGTH_SHORT).show();
                            if (saveCheck.isChecked()){
                                Utilities.instance().storeValueString("uid", fAuth.getCurrentUser().getUid());
                                Utilities.instance().storeValueString("email", email);
                                Utilities.instance().storeValueString("pass", password);
                            }
                            startActivity(new Intent(getApplicationContext(),MainActivity.class));
                        }else {
                            Toast.makeText(LoginActivity.this, "Errore! Inserire una email e password corrette", Toast.LENGTH_SHORT).show();
                            progressBar.setVisibility(View.GONE);
                        }

                    }
                });

            }
        });

        mRegisterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), RegisterActivity.class));
            }
        });

        Context ct = this;

        final EditText resetMail = new EditText(this);

        forgotTextLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SweetAlertDialog passwordResetDialog = new SweetAlertDialog(ct, SweetAlertDialog.NORMAL_TYPE);
                passwordResetDialog.setTitleText("Inserisci una mail di recupero")
                .setContentText("Enter Your Email To Received Reset Link.")
                .setCustomView(resetMail)
                .setConfirmButton("Si", dialog -> {
                    String mail = resetMail.getText().toString();
                    fAuth.sendPasswordResetEmail(mail)
                            .addOnSuccessListener(aVoid ->
                                    Toast.makeText(LoginActivity.this, "Link di Reset inviato.", Toast.LENGTH_SHORT).show())
                            .addOnFailureListener(e ->
                                    Toast.makeText(LoginActivity.this, "Errore! Inserire una mail valida." + e.getMessage(), Toast.LENGTH_SHORT).show());
                });
                passwordResetDialog.setCancelable(true);
                passwordResetDialog.setCancelText("Annulla");
                passwordResetDialog.show();
            }
        });
    }

    @Override
    protected  void onResume() {
        super.onResume();
        String emailText = (Utilities.instance().fetchValueString("email"));
        String passText = (Utilities.instance().fetchValueString("pass"));

        if (!emailText.isEmpty() && !emailText.isEmpty()) {
            mEmail.setText(emailText);
            mPassword.setText(passText);
        }
    }

    public void animation(){
        final Animation animation;
        if (dir == 1){
            animation = AnimationUtils.loadAnimation(this, R.anim.bounce_left);
            dir = 0;
        }else{
            animation = AnimationUtils.loadAnimation(this, R.anim.bounce);
            dir = 1;
        }
        BounceInterpolator interpolator = new BounceInterpolator();
        animation.setInterpolator(interpolator);
        logoImage.startAnimation(animation);
    }

    public void restartTimer(){
        timer.cancel();
        time = 5;
        animation();
        timer.start();
    }

    public void onBackPressed() {
        SweetAlertDialog dialog = new SweetAlertDialog(this);
        dialog.setTitleText("Uscire dal gioco?");
        dialog.setContentText("");
        dialog.setConfirmButton("Si", sweetAlertDialog -> {
            finish();
        });
        dialog.setCancelable(true);
        dialog.setCancelText("Annulla");
        dialog.show();
    }
}
