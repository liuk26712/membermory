package it.liuk.membermory;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.BounceInterpolator;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.jinatonic.confetti.CommonConfetti;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class RankingActivity extends AppCompatActivity {
    public static final String TAG = "TAG";

    RecyclerView rv;
    TextView userText, levelText, scoreText;
    ConstraintLayout container;

    FirebaseFirestore db = FirebaseFirestore.getInstance();
    Adapter testAdapter;
    int liv;
    int score;
    String user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ranking);
        rv = findViewById(R.id.recycler_view);
        container = findViewById(R.id.container);
        userText = findViewById(R.id.user_mine);
        levelText = findViewById(R.id.level_mine);
        scoreText = findViewById(R.id.score_mine);


        Utilities.instance(this.getApplicationContext());
        Utilities.instance().checkInternet(this);
        user = Utilities.instance().fetchValueString("fName");
        userText.setText(user);
        liv = Utilities.instance().fetchValueInt("level");
        levelText.setText(Integer.toString(liv));

        score = Utilities.instance().fetchValueInt("score");
        int n = score + 1;
        n = n -1;
        scoreText.setText(Integer.toString(n));

        Ranked();
    }

    private void isEnterTable(List<User> highScores) {
        boolean classifica = false;
        boolean podio = false;

        for (int i = 0; i < highScores.size(); i++){
            if (user.equals(highScores.get(i).getName())){
                classifica = true;
                if (i < 3){
                    podio = true;
                }
            }
        }

        if (podio) {
            new Handler().postDelayed(() -> runOnUiThread(() ->
                    CommonConfetti.rainingConfetti(container, new int[]{Color.YELLOW, Color.RED, Color.BLUE, Color.GREEN}).stream(7000)), 100);

            Toast.makeText(RankingActivity.this, getString(R.string.in_podio), Toast.LENGTH_LONG).show();

        }
        else if (classifica){
            Toast.makeText(RankingActivity.this, getString(R.string.in_table), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(RankingActivity.this, getString(R.string.not_in_table), Toast.LENGTH_LONG).show();
        }
    }

    public void  InsertInView(List<User> userList){
        testAdapter = new Adapter(userList);
        isEnterTable(userList);
        rv.setLayoutManager(new LinearLayoutManager(RankingActivity.this));
        rv.setAdapter(testAdapter);
    }

    public void Ranked(){
        final ArrayList<User> userList = new ArrayList<>();

        db.collection("users").get().addOnCompleteListener(task -> {
            for (QueryDocumentSnapshot qs : task.getResult()) {
                final String[] fName = new String[3];
                User u = new User();
                qs.getData().forEach((s, o) -> {
                    if (s.equals("fName")){
                        fName[0] = o.toString();
                        u.setName(fName[0]);
                    }
                    if (s.equals("level")){
                        fName[1] = o.toString().replaceAll("\"","");
                        u.setLevel(Integer.parseInt(fName[1]));
                    }
                    if (s.equals("score")){
                        fName[2] = o.toString().replaceAll("\"","");
                        u.setScore(Integer.parseInt(fName[2]));
                    }
                    if (fName[0] != null && fName[1] != null && fName[2] != null){
                        userList.add(u);
                    }
                });
            }

            List<User> result = userList.stream()
                    .distinct()
                    .limit(15)
                    .sorted(Comparator.comparingInt(User::getScore).reversed())
                    .collect(Collectors.toList());

            InsertInView(result);

        }).isComplete();
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
        finish();
    }
}

