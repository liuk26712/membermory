package it.liuk.membermory;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class MainActivity extends AppCompatActivity {
    FirebaseAuth fAuth;
    FirebaseFirestore fStore;
    String userId;
    Button profileBtn, gameBtn, rankingBtn, logoutBtn;
    TextView lvText, fNameText, scoreText;
    ProgressBar pb;
    FirebaseUser user;
    int level;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lvText = findViewById(R.id.lvText);
        scoreText = findViewById(R.id.scoreText);
        gameBtn = findViewById(R.id.gameBtn);
        profileBtn = findViewById(R.id.profileBtn);
        rankingBtn = findViewById(R.id.RankingBtn);
        logoutBtn = findViewById(R.id.logOutBtn);
        fNameText = findViewById(R.id.fName);
        pb = findViewById(R.id.progressBarLoading);

        Utilities.instance(this.getApplicationContext());
        Utilities.instance().checkInternet(this);
        lvText.setVisibility(View.INVISIBLE);

        fAuth = FirebaseAuth.getInstance();
        if(fAuth.getCurrentUser() == null){
            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            finish();
        }

        fStore = FirebaseFirestore.getInstance();
        userId = fAuth.getCurrentUser().getUid();
        user = fAuth.getCurrentUser();

        gameBtn.setOnClickListener(v -> {
            if (!user.isEmailVerified()) {
                Toast.makeText(getBaseContext(), "Completare il profilo per giocare.", Toast.LENGTH_LONG).show();
            }else {
                startActivity(new Intent(getApplicationContext(), GameActivity.class)
                        .putExtra("LEVEL", level));
            }
        });

        profileBtn.setOnClickListener(v -> startActivity(new Intent(getApplicationContext(), ProfileActivity.class)));

        rankingBtn.setOnClickListener(v -> startActivity(new Intent(getApplicationContext(), RankingActivity.class)));

        logoutBtn.setOnClickListener(v -> {
            FirebaseAuth.getInstance().signOut();//logout
            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            finish();
        });

        update();
    }

    public void update(){

        DocumentReference docRef = fStore.collection("users").document(userId);
        docRef.get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                DocumentSnapshot document = task.getResult();
                assert document!=null;
                if (document.exists()) {
                    fNameText.setText((CharSequence) document.getData().get("fName"));
                    lvText.setText((CharSequence) document.getData().get("level"));
                    scoreText.setText((CharSequence) document.getData().get("score"));
                }
            }

            String text = fNameText.getText().toString() + "";
            Utilities.instance().storeValueString("fName", text);

            pb.setVisibility(View.GONE);
            lvText.setVisibility(View.VISIBLE);
            text = lvText.getText().toString() + "";
            level = Integer.parseInt(text);
            Utilities.instance().storeValueInt("level", level);

            text = scoreText.getText().toString() + "";
            level = Integer.parseInt(text);
            Utilities.instance().storeValueInt("score", level);

        }).isComplete();
    }

    @Override
    public void onResume(){
        super.onResume();
        update();
    }

    @Override
    public void onBackPressed(){
        SweetAlertDialog dialog = new SweetAlertDialog(this, SweetAlertDialog.NORMAL_TYPE);
        dialog.setTitleText("Uscire dal gioco?");
        dialog.setContentText("Verrai deautenticato dal gioco.");
        dialog.setConfirmButton("Si", sweetAlertDialog -> {
            FirebaseAuth.getInstance().signOut();//logout
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        });
        dialog.setCancelable(true);
        dialog.setCancelText("Annulla");
        dialog.show();
    }


    @Override
    public void onStart(){
        super.onStart();
    }
}
